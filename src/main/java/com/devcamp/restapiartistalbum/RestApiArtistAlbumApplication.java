package com.devcamp.restapiartistalbum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiArtistAlbumApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiArtistAlbumApplication.class, args);
	}

}
